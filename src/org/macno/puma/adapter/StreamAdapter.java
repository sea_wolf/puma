package org.macno.puma.adapter;

import java.util.List;

import org.macno.puma.core.Stream;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class StreamAdapter extends ArrayAdapter<Stream> {

	private int mResource;
	private Context mContext;
	
	public StreamAdapter(Context context, int resource, List<Stream> a ) {
		super(context, resource,a);
		mResource = resource;
		mContext=context;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Stream s = getItem(position);
		View v = null;
		if(v==null) {
	        LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        v=vi.inflate(mResource, null);
	    }
		TextView tv = (TextView)v;
		tv.setText(s.name);
		return v;
	}
}
