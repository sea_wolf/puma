package org.macno.puma.fragment;

import static org.macno.puma.PumaApplication.APP_NAME;
import static org.macno.puma.PumaApplication.DEBUG;

import java.lang.ref.WeakReference;

import org.macno.puma.R;
import org.macno.puma.activity.SettingsActivity;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

public class SettingsFragment extends PreferenceFragment implements
		OnSharedPreferenceChangeListener {

	// private static final String TAG = "Booktab.PF";

	private SharedPreferences prefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);

		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

		setPreferenceSummary(getString(R.string.pref_gridview));
	}

	SettingsHandler mHandler = new SettingsHandler(this);

	static class SettingsHandler extends Handler {

		private final WeakReference<SettingsFragment> mTarget;

		private final static int MSG_RESET_COMPLETE = 0;

		SettingsHandler(SettingsFragment target) {
			mTarget = new WeakReference<SettingsFragment>(target);
		}

		public void handleMessage(Message msg) {
			SettingsFragment target = mTarget.get();
			if (target == null) {
				return;
			}
			switch (msg.what) {
			case MSG_RESET_COMPLETE:
				break;
			}

		}

	}

	private void setPreferenceSummary(String key) {

		if (prefs.contains(key)) {
			if (key.equals(getString(R.string.pref_gridview))) {
				boolean on = prefs.getBoolean(key, true);
				findPreference(key).setSummary(
						on ? R.string.pref_gridview_summ_on
								: R.string.pref_gridview_summ_off);

			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if (DEBUG) {
			Log.d(APP_NAME, "onSharedPreferenceChanged: " + key);
		}

		setPreferenceSummary(key);

		((SettingsActivity) getActivity()).setNeedRefresh();
	}
}
