package org.macno.puma.fragment;

import static org.macno.puma.PumaApplication.APP_NAME;
import static org.macno.puma.PumaApplication.DEBUG;

import org.macno.puma.R;
import org.macno.puma.activity.HomeActivity;
import org.macno.puma.adapter.ActivityAdapter;
import org.macno.puma.core.Account;
import org.macno.puma.core.Stream;
import org.macno.puma.manager.AccountManager;
import org.macno.puma.manager.StreamManager;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.etsy.android.grid.StaggeredGridView;


public class StreamFragment extends Fragment {

	public static final String ARG_STREAM_ID = "extraStreamId";
	
	private Account mAccount;
	private Stream mStream;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String accountUUID = "";
		if (savedInstanceState != null) {
			mStream = StreamManager.getStream(savedInstanceState.getString(ARG_STREAM_ID));
			accountUUID = savedInstanceState.getString(HomeActivity.EXTRA_ACCOUNT_UUID);
		} else {
			mStream = StreamManager.getStream(getArguments().getString(ARG_STREAM_ID));
			accountUUID = getArguments().getString(HomeActivity.EXTRA_ACCOUNT_UUID);
		}
		
		AccountManager am = new AccountManager(getActivity());
		mAccount = am.getAccount(accountUUID);

	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putString(ARG_STREAM_ID, mStream.id);
		outState.putString(HomeActivity.EXTRA_ACCOUNT_UUID,mAccount.getUuid());
		super.onSaveInstanceState(outState);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
		
//        View layout = inflater.inflate(R.layout.stream_list,null);
//		ListView activityList = (ListView)layout.findViewById(R.id.activities_list);

		View layout;
		 
        ActivityAdapter activityAdapter = new ActivityAdapter(getActivity(),mAccount,mStream.id);
		mStream.adapter = activityAdapter;
		
		boolean useGrid = getResources().getBoolean(R.bool.useGrid);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		boolean gridEnabled = prefs.getBoolean(getString(R.string.pref_gridview),true);
		if(useGrid && gridEnabled) {
			layout = new StaggeredGridView (getActivity());
			layout.setBackgroundColor(Color.BLACK);
			
			((StaggeredGridView)layout).setAdapter(activityAdapter);
			((StaggeredGridView)layout).setOnScrollListener(activityAdapter);
		} else {
			if(DEBUG)
				Log.d(APP_NAME,"Using ListView");
			layout = new ListView(getActivity());
			((ListView)layout).setOnScrollListener(activityAdapter);
			((ListView)layout).setAdapter(activityAdapter);
		}
		
		
		
//		container.addView(layout);
		return layout;
		
    }
	
	public void clearCache() {
		mStream.adapter.clearCache();
	}
	
	public void reload() {
		mStream.adapter.checkNewActivities();
	}
}
