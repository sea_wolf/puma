package org.macno.puma.manager;

import java.util.ArrayList;

import org.macno.puma.core.Stream;

public class StreamManager {

	public static final ArrayList<Stream> getStreams() {
		ArrayList<Stream> mStreamNames = new ArrayList<Stream>();
        Stream inbox = new Stream();
        inbox.id="inbox/major";
        inbox.name="Inbox";
        mStreamNames.add(inbox);
 
        Stream direct = new Stream();
        direct.id="inbox/direct";
        direct.name="Messages";
        mStreamNames.add(direct);
 
        Stream minor = new Stream();
        minor.id="inbox/minor";
        minor.name="Meanwhile";
        mStreamNames.add(minor);
 
        Stream feed = new Stream();
        feed.id="feed/major";
        feed.name="Activity";
        mStreamNames.add(feed);
        
        Stream publicFeed = new Stream();
        publicFeed.id="https://ofirehose.com/feed.json";
        publicFeed.name="OFirehose.com feed";
        mStreamNames.add(publicFeed);
        
        return mStreamNames;
	}
	
	public static Stream getStream(String id) {
		for (Stream stream : getStreams()) {
			if(stream.id.equals(id)) {
				return stream;
			}
		}
		return null;
	}
}
